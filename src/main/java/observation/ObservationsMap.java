package observation;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


/**
 * The observations hash map class, which allows iterating over
 * the listed stations in the stations.config file with the method nextObservation().
 */
public class ObservationsMap<K, V> extends HashMap<String, Observation> {

    private static final Logger log = LoggerFactory.getLogger(ObservationsMap.class);

    /**
     * Static index that will be shared by every new map created.
     * Needed so that the index won't be reset, when a new
     * map is created that contains new observations
     */
    private static int index = 0;
    private final String STATIONS_CONFIG_FILE = "/stations.config";
    /**
     * Static stations list, that will be read only once from the config file
     * when the application is run.
     */
    private static List<String> stations = new ArrayList<String>();

    public ObservationsMap() {
        loadStationsConfig();
    }

    private void loadStationsConfig() {
        if (stations.isEmpty()) {
            try {
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(
                                this.getClass().getResourceAsStream(STATIONS_CONFIG_FILE),
                                StandardCharsets.ISO_8859_1));

                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    stations.addAll(Arrays.asList(line.split(",")));
                }
            } catch (IOException e) {
                log.debug("Exception reading config, {}", e);
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns the next observation according to the stations read from the config file.
     * If the stations does not exist in the fetched objects, empty observation is returned.
     *
     * @return an observation
     */
    public Observation nextObservation() {
        if (index == stations.size() - 1) {
            index = 0;
        } else {
            index++;
        }
        String stationName = stations.get(index);
        Observation o = this.get(stationName);
        if (o == null) {
            o = new Observation();
            o.setStationName(stationName);
        }
        return o;
    }

    /**
     * Get the current observation
     *
     * @return an observation
     */
    public Observation getCurrentObservation() {
        return this.get(stations.get(index));
    }


}
