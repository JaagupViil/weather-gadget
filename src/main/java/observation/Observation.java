package observation;


import org.w3c.dom.Node;


/**
 * Class representing a weather observation.
 */
public class Observation {

    /**
     * XML tags that correspond to a observation in www.ilmateenistus.ee XML response
     */
    public static final String STATION_NAME_TAG = "name";
    public static final String TEMPERATURE_TAG = "airtemperature";
    public static final String PHENOMENON_TAG = "phenomenon";

    private String stationName = "";
    private Double temperature = 0.0;
    private String phenomenon = "default";

    public Observation() {}

    /**
     * Constructor that takes the XML node elements and constructs an observation.
     *
     * @param station
     * @param temperature
     * @param phenomenon
     */
    public Observation(Node station, Node temperature, Node phenomenon) {
        this.stationName = station.getNodeValue();
        this.temperature =
                temperature != null ? Double.parseDouble(temperature.getNodeValue()) : 0.0;
        this.phenomenon = phenomenon != null ? phenomenon.getNodeValue() : "default";
    }


    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "Observation{" +
                "stationName='" + stationName + '\'' +
                ", temperature=" + temperature +
                ", phenomenon='" + phenomenon + '\'' +
                '}';
    }

    public String getPhenomenon() {
        return phenomenon;
    }

    /**
     * Transforms a phenomenon string like 'CLEAR NIGHT'  in to 'clear_night'
     * @return the formatted string with underscores and in lower case.
     */
    public String getFormattedPhenomenon() {
        return this.phenomenon.toLowerCase().replaceAll(" ", "_");
    }

}
