import observation.Observation;
import service.ImageService;
import service.ObservationService;

/**
 * The main entry of the weather-app
 */
public class Main {
    public static void main(String[] args) {
        new GadgetPanel(new ImageService(new Observation()),new ObservationService());
    }
}
