package service;


import observation.Observation;
import observation.ObservationsMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;


/**
 * The service responsible for fetching the observations from a weather service url.
 */
public class ObservationService {

    private static final Logger log = LoggerFactory.getLogger(ObservationService.class);

    private static final String USER_AGENT = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) "
            + "Gecko/20100101 Firefox/54.0";

    /**
     * The WEATHER_SERVICE_URL to fetch the observations from
     */
    private static final String WEATHER_SERVICE_URL =
            "http://www.ilmateenistus.ee/ilma_andmed/xml/observations.php";
    /**
     * THE XML tag that separates each station, i.e., observation
     */
    private static final String STATION_TAG = "station";

    private DocumentBuilder db;

    public ObservationService() {
        log.debug("Creating observation service");
        try {
            db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Goes over the weather service url and fetches the observations from the XML.
     * If there's a problem with the connection, retries to fetch the observations every 3 seconds.
     * The XML returned is in the format of: <br/>
     * <p>
     * {@code <station>}<br/>
     * {@code <name>..</name>}<br/>
     * {@code <airtemperature>..</airtemperature>}<br/>
     * ..<br/>
     * ..<br/>
     * {@code <windsped>..</windsped>}<br/>
     * {@code </station>}<br/>
     *
     * @return hashmap that has the station name as a key and the observation as a value
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public ObservationsMap<String, Observation> fetchObservations() {
        log.debug("Starting to fetch the observations from: {}", WEATHER_SERVICE_URL);
        ObservationsMap<String, Observation> observations = new ObservationsMap<>();
        InputStream in = null;
        InputStreamReader inputStreamReader = null;
        try {
            URLConnection urlConnection = new URL(WEATHER_SERVICE_URL).openConnection();
            urlConnection.setRequestProperty("User-Agent", USER_AGENT);
            in = urlConnection.getInputStream();
            inputStreamReader = new InputStreamReader(in, StandardCharsets.ISO_8859_1);

            Document doc = db.parse(new InputSource(inputStreamReader));
            doc.getDocumentElement().normalize();
            NodeList stations = doc.getElementsByTagName(STATION_TAG);
            for (int i = 0; i < stations.getLength(); i++) {
                Node station = stations.item(i);
                Element element = (Element) station;
                Observation observation = createObservation(element);
                observations.put(observation.getStationName(), observation);
            }
            in.close();
        } catch (Exception e) {
            log.debug("Exception while fetching observations", e);
            try {
                //Finally close the readers to be sure
                if (in != null && inputStreamReader != null) {
                    in.close();
                    inputStreamReader.close();
                }
                //Sleep 3 seconds and try to fetch the observations again
                log.debug("Sleeping 3 seconds...");
                Thread.sleep(3000);
                observations = fetchObservations();
            } catch (Exception e1) {
                log.error("Exception: ", e1);
            }
        }
        return observations;
    }

    /**
     * Creates an observation from a XML document element
     *
     * @param stationElement document element
     * @return an observation
     */
    private Observation createObservation(Element stationElement) {
        Node station = getElementsFirstChildByTag(Observation.STATION_NAME_TAG, stationElement);
        Node temperature = getElementsFirstChildByTag(Observation.TEMPERATURE_TAG, stationElement);
        Node phenomenon = getElementsFirstChildByTag(Observation.PHENOMENON_TAG, stationElement);
        return new Observation(station, temperature, phenomenon);
    }

    private Node getElementsFirstChildByTag(String tag, Element element) {
        return element.getElementsByTagName(tag).item(0).getFirstChild();
    }

}
