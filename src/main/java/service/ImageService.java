package service;


import observation.Observation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * The service which is responsible for fetching and serving the images from the resources folder.
 */
public class ImageService {

    private static final Logger log = LoggerFactory.getLogger(ImageService.class);

    private Observation observation;
    private Image backgroundImage;
    private Image phenomenonImage;
    private Image exitButtonImage;

    public ImageService(Observation observation) {
        this.observation = observation;
        this.exitButtonImage = getImageFromResources("/background/exit_btn.png");
        initBackgroundImage();
        initPhenomenonImage();
    }

    public ImageService update(Observation newObservation) {
        log.debug("Updating image service");
        Observation oldObservation = observation;
        this.observation = newObservation;
        if (!newObservation.getFormattedPhenomenon().equals(oldObservation.getFormattedPhenomenon())) {
            initPhenomenonImage();
        }
        //If the temperature range differs
        if (oldObservation.getTemperature() < 15 && newObservation.getTemperature() >= 15
                || oldObservation.getTemperature() > 15 && newObservation.getTemperature() <= 15) {
            initBackgroundImage();
        }
        return this;
    }

    /**
     * Load the background image depending on the temperature
     */
    private void initBackgroundImage() {
        String imagePath = "/background";
        if (observation.getTemperature() > 15) {
            imagePath += "/warm.png";
        } else {
            imagePath += "/cold.png";
        }
        backgroundImage = getImageFromResources(imagePath);
        log.debug("Initializing background image: {}", imagePath);
    }

    /**
     * Fetches the phenomenon image from the resources folder.
     * If the day has ended, and the phenomenon is either clear or cloudy_with_clear_spells, the
     * keyword "_night" will be appended to the picture to be fetched, i.e., clear_night.
     */
    private void initPhenomenonImage() {
        String underScoreName = observation.getFormattedPhenomenon();
        //to lower and replace spaces with underscore
        if (isNight() && underScoreName.contains("clear")) {
            underScoreName += "_night";
        }
        log.debug("Initializing phenomen image: {}", underScoreName);
        phenomenonImage = getImageFromResources("/phenomenon/" + underScoreName + ".png");

    }

    /**
     * Night time is from 24.00 - 07.00
     *
     * @return true if currently it is night or not
     */
    private boolean isNight() {
        int hour = GregorianCalendar.getInstance().get(Calendar.HOUR_OF_DAY);
        return hour >= 24 && hour <= 7;
    }

    private Image getImageFromResources(String name) {
        Image img = null;
        try {
            img = ImageIO.read(this.getClass().getResourceAsStream(name));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }

    public Image getBackgroundImage() {
        return backgroundImage;
    }

    public Image getPhenomenonImage() {
        return phenomenonImage;
    }

    public Image getExitButtonImage() {
        return exitButtonImage;
    }
}
