import observation.Observation;
import observation.ObservationsMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.ImageService;
import service.ObservationService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Timer;
import java.util.TimerTask;


/**
 * The class where the weather-gadget is drawn to the screen.
 */
public class GadgetPanel extends JPanel {

    private static final Logger log = LoggerFactory.getLogger(GadgetPanel.class);
    /**
     * The service responsible for fetching the observations from Estonian's weather service.
     */
    private ObservationService observationService;
    private ObservationsMap<String, Observation> observations = new ObservationsMap<>();

    private ImageService imageService;
    private Timer timer = new Timer(true);

    private JDialog frame;
    /**
     * The fonts of the text on the gadget
     */
    private Font smallFont = new Font("Arial", Font.PLAIN, 15);
    private Font bigFont = new Font("Arial", Font.BOLD, 25);


    private boolean mouseEntered = false;
    private boolean updatingObservations = false;

    public GadgetPanel(ImageService imageService, ObservationService observationService) {
        this.imageService = imageService;
        this.observationService = observationService;
        fetchTemperaturesPeriodically();
        createFrame();
    }

    private void createFrame() {
        log.debug("Creating the gadget's frame");
        int width = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        frame = new JDialog();
        frame.add(this);
        frame.setUndecorated(true); //no window
        frame.setBackground(new Color(0, 0, 0, 0)); //transparent
        frame.setLocation(width - 155, 20);
        frame.setSize(150, 60);
        frame.setVisible(true);
        frame.setFocusableWindowState(false); //can't be focused
        frame.addMouseListener(buildMouseListener());
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.WHITE);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        if (mouseEntered) {
            g2d.drawImage(imageService.getExitButtonImage(), 135, 2, this);
        }
        if (updatingObservations) {
            drawUpdatingMsg(g2d);
        } else {
            drawObservation(g2d, observations.getCurrentObservation());
        }
    }

    private void drawObservation(Graphics2D g, Observation observation) {
        g.drawImage(imageService.getBackgroundImage(), 0, 0, this);
        g.drawImage(imageService.getPhenomenonImage(), 90, 2, this); // Draw phenomenon image
        g.setFont(smallFont);
        g.drawString(observation.getStationName(), 5, 16);
        g.setFont(bigFont);
        g.drawString(observation.getTemperature() + " °C", 5, 41);
    }

    private void drawUpdatingMsg(Graphics2D g) {
        g.drawImage(imageService.getBackgroundImage(), 0, 0, this);
        g.setFont(bigFont);
        g.drawString("Updating", 5, 41);
    }

    private MouseListener buildMouseListener() {
        return new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) { //change to next station on left click
                    log.debug("Clicked left mouse button, changing the weather station");
                    imageService = imageService.update(observations.nextObservation());
                    frame.repaint();
                } else {                                   //update the observations on right click
                    log.debug("Clicked right mouse button");
                    updatingObservations = true;
                    frame.repaint();
                    javax.swing.Timer timer = new javax.swing.Timer(500, listener ->  updateObservations());
                    timer.setRepeats(false);
                    timer.start();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (frame.getMousePosition().x >= 135 && frame.getMousePosition().y <= 15) {
                    log.info("Exiting..");
                    System.exit(0);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                mouseEntered = true;
                frame.repaint();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                mouseEntered = false;
                frame.repaint();
            }
        };
    }

    /**
     * Fetch the observations every hour.
     */
    private void fetchTemperaturesPeriodically() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                updateObservations();
            }
        }, 0, 1000 * 3600); //update every hour
    }

    private void updateObservations() {
        log.info("Updating observations");
        updatingObservations = true;
        observations = observationService.fetchObservations();
        updatingObservations = false;
        imageService = imageService.update(observations.getCurrentObservation());
        log.info("New current observation: {}", observations.getCurrentObservation());
        frame.repaint();
    }
}
